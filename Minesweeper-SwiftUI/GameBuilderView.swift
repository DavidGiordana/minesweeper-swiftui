//
//  GameBuilderView.swift
//  Minesweeper-SwiftUI
//
//  Created by David Giordana on 8/25/21.
//

import SwiftUI

struct GameBuilderView: View {

    @State private var rows = Constants.minTiles
    @State private var columns = Constants.minTiles
    @State private var minesCount = Constants.minMines
    
    var body: some View {
        NavigationView {
            VStack {
                stepperView(value: $rows, range: Constants.tilesRange, label: "Rows")
                stepperView(value: $columns, range: Constants.tilesRange, label: "Columns")
                stepperView(value: $minesCount, range: Constants.minMines...maxMines, label: "Mines")
                
                Spacer()
                
                NavigationLink(destination: gameplayView) { startGameButton }
            }
            .navigationTitle("Create Minesweeper")
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    private func stepperView(value: Binding<Int>, range: ClosedRange<Int>, label: String) -> some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.blue.opacity(0.1))
        
            Stepper(
                value: value,
                in: range
            ) {
                Text("\(label): \(value.wrappedValue)\t")
            }
            .padding(.vertical, 10)
            .padding(.horizontal, 15)
        }
        .fixedSize(horizontal: false, vertical: true)
        .padding(.horizontal, 10)
        .padding(.bottom, 5)
    }
    
    private var maxMines: Int {
        return max(Int(Double(rows * columns) * Constants.maxMinesRate), 1)
    }
    
    private var startGameButton: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.blue)
        
            Text("Start Game")
                .padding(.vertical, 10)
                .padding(.horizontal, 15)
                .foregroundColor(.white)
                .font(Font.bold(.headline)())
        }
        .fixedSize(horizontal: false, vertical: true)
        .padding(.horizontal, 20)
    }
    
    private var gameplayView: some View {
        GameplayView()
            .environmentObject(Minesweeper(rows: rows, columns: columns, minesCount: minesCount))
    }
 
    private struct Constants {
        static let minTiles = 5
        static let tilesRange = 5...25
        static let minMines = 5
        static let maxMinesRate = 0.8
    }
    
}

struct GameBuilderView_Previews: PreviewProvider {
    static var previews: some View {
        GameBuilderView()
            .previewDevice(.init(stringLiteral: "iPhone 11"))
    }
}
