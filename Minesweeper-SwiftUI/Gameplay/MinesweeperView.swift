//
//  MinesweeperView.swift
//  Minesweeper_SwiftUIApp
//
//  Created by David Giordana on 8/25/21.
//

import SwiftUI

struct MinesweeperView: View {
    
    @EnvironmentObject var game: Minesweeper
        
    var body: some View {
        GeometryReader { geometry in
            let tileSize = calculateTileSize(from: geometry.size)
            let width = tileSize * CGFloat(game.columns)
            let height = tileSize * CGFloat(game.rows)
            
            ZStack {
                LazyVGrid(
                    columns: columns(tileSize: tileSize),
                    alignment:.leading,
                    spacing: 0
                ) {
                    ForEach(game.grid) { cell in
                        TileView(cell: cell, tileSize: tileSize)
                            .onTapGesture {
                                withAnimation(.linear(duration: 0.2)) {
                                    game.toggle(cell: cell)
                                }
                            }
                            .onLongPressGesture(minimumDuration: 0.1) {
                                game.toggleFlag(cell: cell)
                            }
                    }
                }
                
                
                if game.gameOver {
                    GameOverView()
                }
            }
            .background(Color.white)
            .frame(width: width, height: height)
            .position(
                x: geometry.size.width / 2,
                y: geometry.size.height / 2
            )
        }
    }
    
    // MARK: - Internal
    
    private func calculateTileSize(from size: CGSize) -> CGFloat {
        let rows = CGFloat(game.rows)
        let cols = CGFloat(game.columns)
        
        return min(size.width / cols, size.height / rows)
    }
    
    private func columns(tileSize: CGFloat) -> [GridItem] {
        return (1...game.columns).map { _ in
            .init(.fixed(tileSize), spacing: 0)
        }
    }
    
}

struct TileView: View {
    
    let cell: Minesweeper.Cell
    let tileSize: CGFloat
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Constants.tileColor(isCovered: cell.covered))
                .border(Constants.tileBorderColor(isCovered: cell.covered))
                
            if cell.covered, cell.flagged {
                text("⛳️")
            }
            else if (!cell.covered) {
                switch cell.type {
                case .mine: text("💥")
                case .count(let count): text(count == 0 ? "" : "\(count)")
                }
            }
        }
        .frame(width: tileSize, height: tileSize)
    }
    
    private func text(_ text: String) -> some View {
        Text(text)
            .font(.system(size: tileSize * Constants.fontScale))
    }
    
    // MARK: - Constants
    
    private struct Constants {
        
        static let fontScale: CGFloat = 0.7
        
        static func tileColor(isCovered: Bool) -> Color {
            return isCovered ? Color.blue : Color.blue.opacity(0.5)
        }
        
        static func tileBorderColor(isCovered: Bool) -> Color {
            return isCovered ? Color.black.opacity(0.5) : Color.blue
        }
    }
    
}

struct GameOverView: View {
    
    @EnvironmentObject var game: Minesweeper
    
    var body: some View {
        let text = game.gameplayStatus == .win ? "YOU WIN" : "GAME OVER"
        ZStack {
            Color.white.opacity(0.5)
            
            Text(text)
                .font(Font.bold(.largeTitle)())
                .padding(20)
                .foregroundColor(Color.white)
                .background(roundedBackground)
        }
    }
    
    var roundedBackground: some View {
        RoundedRectangle(cornerRadius: 25.0)
            .stroke(Color.black.opacity(0.25), lineWidth: 3)
            .background(
                RoundedRectangle(cornerRadius: 25.0)
                    .foregroundColor(.blue)
            )
    }
    
}

/*
struct MinesweeperView_Previews: PreviewProvider {
    static var previews: some View {
        /*MinesweeperView()
            .previewDevice(.init(stringLiteral: "iPhone 11"))*/
    }
}*/
