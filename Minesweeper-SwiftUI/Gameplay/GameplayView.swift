//
//  GameplayView.swift
//  Minesweeper_SwiftUIApp
//
//  Created by David Giordana on 8/24/21.
//

import SwiftUI

struct GameplayView: View {
    
    @Environment(\.presentationMode) var presentation
    
    @EnvironmentObject var game: Minesweeper
    
    var body: some View {
        VStack(alignment: .center) {            
            MinesweeperView()
            resetButton
        }
        .padding()
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
           Image(systemName: "arrow.left")
                .onTapGesture {
                    self.presentation.wrappedValue.dismiss()
                    self.game.reset()
                }
        )
    }
    
    var resetButton: some View {
        Button {
            game.reset()
        } label: {
            Text("Reset")
        }
    }
    
}

struct GameplayView_Previews: PreviewProvider {
    static var previews: some View {
        GameplayView()
            .previewDevice(.init(stringLiteral: "iPhone 11"))
    }
}
