//
//  Model.swift
//  Minesweeper_SwiftUIApp
//
//  Created by David Giordana on 8/25/21.
//

import Foundation

class Minesweeper: ObservableObject {
    
    // MARK: Type
    
    enum CellType: Equatable, Hashable {
        case mine, count(Int)
    }
    
    struct Cell: Identifiable, Hashable {
        
        var id: String { "\(row)-\(col)" }
        
        var row: Int
        var col: Int
        
        var type: CellType
        
        var flagged = false
        var covered = true
        
        var isMine: Bool { type == .mine }
        
    }
    
    enum GameplayStatus: Equatable {
        case playing, win, lose
    }
    
    // MARK: - Data
    
    @Published private(set) var grid: Grid<Cell>
    
    let minesCount: Int
    
    var rows: Int { grid.rows }
    
    var columns: Int { grid.columns }
    
    private(set) var gameplayStatus = GameplayStatus.playing
    
    var gameOver: Bool {
        return gameplayStatus != .playing
    }
    
    // MARK: - Init
    
    init(rows: Int, columns: Int, minesCount: Int) {
        self.minesCount = minesCount
        self.grid = Minesweeper.createMinesweeperGrid(rows: rows, columns: columns, minesCount: minesCount)
    }

    static func createMinesweeperGrid(rows: Int, columns: Int, minesCount: Int) -> Grid<Minesweeper.Cell> {
        // Create Base Grid
        let defaultValue = Minesweeper.Cell(row: 0, col: 0, type: .count(0))
        var grid = Grid(rows: rows, columns: columns, defaultValue: defaultValue)
        
        // Create Mines
        (0..<minesCount).forEach { grid[$0].type = .mine }
        grid.shuffle()
        
        // Set values
        for row in 0..<rows {
            for col in 0..<columns {
                var element = grid[row, col]
                element.row = row
                element.col = col
                
                if (!element.isMine) {
                    let surroundingMines = grid.surroundingElements(row: row, column: col)
                        .filter { $0.isMine }
                        .count
                    element.type = .count(surroundingMines)
                }
                
                grid[row, col] = element
            }
        }
        
        return grid
    }

    // MARK: - Intent(s)
    
    func toggleFlag(cell: Cell) {
        if cell.covered {
            grid[cell.row, cell.col].flagged = !cell.flagged
        }
    }
    
    func toggle(cell: Cell) {
        if cell.covered, !cell.flagged {
            expansiveUncover(cell: cell)
        }
    }
    
    func reset() {
        self.grid = Minesweeper.createMinesweeperGrid(
            rows: rows,
            columns: columns,
            minesCount: minesCount
        )
        self.gameplayStatus = GameplayStatus.playing
    }
    
    // MARK: - Internal
    
    private func expansiveUncover(cell: Cell) {
        if !cell.covered || cell.flagged { return }
        
        uncover(cell: cell)
        
        if (cell.isMine) {
            self.gameplayStatus = .lose
            uncoverAllMines()
            return
        }
        
        grid
            .crossSurroundingElements(row: cell.row, column: cell.col)
            .forEach { cell in
                if !cell.covered || cell.flagged {
                    return
                }
                
                switch cell.type {
                case .mine: break
                case .count(0): expansiveUncover(cell: cell)
                case .count(_): uncover(cell: cell)
                }
            }
        
        if didWinGame() {
            uncoverAllMines()
            self.gameplayStatus = .win
        }
    }
    
    private func uncover(cell: Cell) {
        self.grid[cell.row, cell.col].covered = false
    }
    
    private func uncoverAllMines() {
        grid
            .filter { $0.isMine && $0.covered }
            .forEach { uncover(cell: $0) }
    }
    
    private func didWinGame() -> Bool {
        return grid
            .filter { !$0.isMine && $0.covered }
            .isEmpty
    }
    
}
