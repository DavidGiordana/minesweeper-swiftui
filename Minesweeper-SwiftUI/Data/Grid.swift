//
//  Matrix.swift
//  Minesweeper_SwiftUIApp
//
//  Created by David Giordana on 8/25/21.
//

import Foundation

struct Grid<Element> {
    
    private var elements: [Element]
    
    let rows: Int
    let columns: Int
    
    init(rows: Int, columns: Int, defaultValue: Element) {
        self.rows = rows
        self.columns = columns
        self.elements = Array(repeating: defaultValue, count: rows * columns)
    }
    
    subscript(row: Int, col: Int) -> Element {
        get {
            let index = row * columns + col
            return elements[index]
        }
        set {
            let index = row * columns + col
            elements[index] = newValue
        }
    }
    
    subscript(index: Int) -> Element {
        get {
            return elements[index]
        }
        set {
            elements[index] = newValue
        }
    }
    
    func any(check: (Element) -> Bool) -> Bool {
        for i in 0..<elements.count {
            if check(elements[i]) {
                return true
            }
        }
        
        return false
    }
    
    mutating func shuffle() {
        self.elements.shuffle()
    }
    
    func surroundingElements(row: Int, column: Int) -> [Element] {
        return [
            (row - 1, column + 1),
            (row - 1, column),
            (row - 1, column - 1),
            (row + 1, column + 1),
            (row + 1, column),
            (row + 1, column - 1),
            (row, column + 1),
            (row, column - 1)
        ]
        .filter { isValidIndex(row: $0, col: $1) }
        .map { self[$0, $1] }
    }
    
    func crossSurroundingElements(row: Int, column: Int) -> [Element] {
        return [
            (row + 1, column),
            (row - 1, column),
            (row, column + 1),
            (row, column - 1)
        ]
        .filter { isValidIndex(row: $0, col: $1) }
        .map { self[$0, $1] }
    }
    
    func filter(filter: (Element) -> Bool) -> [Element] {
        return elements.filter(filter)
    }
    
    // MARK: - Internal
    
    private func isValidIndex(row: Int, col: Int) -> Bool {
        return row >= 0 && row < rows && col >= 0 && col < columns
    }
    
}

extension Grid: RandomAccessCollection {
    var startIndex: Int { 0 }
    var endIndex: Int { rows * columns }
}
