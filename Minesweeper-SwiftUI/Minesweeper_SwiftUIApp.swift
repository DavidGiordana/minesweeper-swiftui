//
//  Minesweeper_SwiftUIApp.swift
//  Minesweeper_SwiftUIApp
//
//  Created by David Giordana on 8/24/21.
//

import SwiftUI

@main
struct Minesweeper_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            GameBuilderView()
        }
    }
}
